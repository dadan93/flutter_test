import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/all_data.dart';

class DataViewModel {
  Future<List<AllDataModel>?> fetchData() async {
    try {
      const String url =
          'http://universities.hipolabs.com/search?country=United+Kingdom';
      final uri = Uri.parse(url);
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        final json = jsonDecode(response.body) as List;
        final data = json.map((e) {
          return AllDataModel(
            alphaTwoCode: e['alpha_two_code'],
            name: e['name'],
            country: e['country'],
            stateProvince: e['state-province'],
            domains:
                (e['domains'] as List).map((item) => item as String).toList(),
            webPages:
                (e['web_pages'] as List).map((item) => item as String).toList(),
          );
        }).toList();
        return data;
      }
      throw (response.statusCode);
    } catch (err) {
      return null;
    }
  }
}
