import 'package:flutter/material.dart';
import 'package:flutter_application_list_test/providers/data_provider.dart';
import 'package:provider/provider.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<DataProvider>(context, listen: false).fetchAllData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home"),
        centerTitle: true,
        backgroundColor: Colors.blueAccent,
        elevation: 0,
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.only(left: 16.0, right: 16.0),
          child: Consumer<DataProvider>(
            builder: (context, value, child) {
              if (value.isLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              final data = value.dataModel;
              return ListView.builder(
                itemCount: data.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      ExpansionTile(
                        title: Text(data[index].name.toString()),
                        children: [
                          SizedBox(
                            height: 30,
                            child: ListTile(
                                title: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Country : ",
                                    style:
                                        Theme.of(context).textTheme.bodyText1),
                                Expanded(
                                  child: Text(data[index].country.toString(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2),
                                ),
                              ],
                            )),
                          ),
                          SizedBox(
                            height: 30,
                            child: ListTile(
                                title: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Web Pages : ",
                                    style:
                                        Theme.of(context).textTheme.bodyText1),
                                Expanded(
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    physics: const ScrollPhysics(),
                                    itemCount: data[index].webPages!.length,
                                    itemBuilder: (context, idx) => Text(
                                        data[index].webPages![idx].toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2),
                                  ),
                                ),
                              ],
                            )),
                          ),
                          SizedBox(
                            height: 30,
                            child: ListTile(
                                title: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("State Province : ",
                                    style:
                                        Theme.of(context).textTheme.bodyText1),
                                Expanded(
                                  child: (data[index].stateProvince == null)
                                      ? const Text("-")
                                      : Text(
                                          data[index].stateProvince.toString(),
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText2),
                                ),
                              ],
                            )),
                          ),
                          ListTile(
                              title: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Domain: ",
                                  style: Theme.of(context).textTheme.bodyText1),
                              Expanded(
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  physics: const ScrollPhysics(),
                                  itemCount: data[index].domains!.length,
                                  itemBuilder: (context, idx) => Text(
                                      " - ${data[index].domains![idx].toString()}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2),
                                ),
                              ),
                            ],
                          )),
                        ],
                      ),
                      const Divider(
                        color: Colors.black,
                      )
                    ],
                  );
                },
              );
            },
          )),
    );
  }
}
