class AllDataModel {
  String? alphaTwoCode;
  String? name;
  String? country;
  List<String>? webPages;
  dynamic stateProvince;
  List<String>? domains;

  AllDataModel(
      {this.alphaTwoCode,
      this.name,
      this.country,
      this.webPages,
      this.stateProvince,
      this.domains});
}
