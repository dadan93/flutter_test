import 'package:flutter/cupertino.dart';

import '../models/all_data.dart';
import '../viewmodels/data_vm.dart';

class DataProvider extends ChangeNotifier {
  final DataViewModel _dataVm = DataViewModel();
  bool isLoading = false;
  List<AllDataModel> _dataModel = [];
  List<AllDataModel> get dataModel => _dataModel;

  Future<void> fetchAllData() async {
    isLoading = true;
    notifyListeners();
    final res = await _dataVm.fetchData();
    _dataModel = res!;
    isLoading = false;
    notifyListeners();
  }
}
